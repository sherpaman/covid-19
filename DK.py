
import pandas as pd
import time

def main():
    today = time.strftime('%Y%m%d')
    data_dir = f'./DK/Data-Epidemiologiske-Rapport-{today}/'

    data_pop = pd.read_csv('./Kommuner_pop_data.csv')
    data = pd.read_csv(f'{data_dir}/Municipality_cases_time_series.csv', sep=';')

    data['Total_Cases'] = data.iloc[:, 1:].sum(axis=1)

    data_reshape = pd.DataFrame(data.set_index('date_sample').stack()).reset_index()
    data_reshape.rename(columns={'level_1': 'Navn', 0: 'infections'}, inplace=True)

    data_rate_ts = pd.merge(right=data_reshape, left=data_pop[['Navn', '2019']], on='Navn').fillna(0.0)
    data_rate_ts['rate'] = data_rate_ts['infections'] / data_rate_ts['2019']
    roll_7 = data_rate_ts.groupby('Navn').rolling(7, on='date_sample').mean().reset_index()[['Navn',
                                                                                              'date_sample',
                                                                                              'rate']]
    roll_7.fillna(0.0, inplace=True)
    roll_7.rename(columns={'rate': 'rolling_rate'}, inplace=True)
    data_rate_ts = pd.merge(right=data_rate_ts, left=roll_7, on=['Navn', 'date_sample'])

    data_rate_ts.to_json('./DK_ts.json')

if __name__ == "__main__":
    main()