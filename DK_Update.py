import os
import re
import time
import zipfile

import requests
from bs4 import BeautifulSoup


def main():

    today = time.strftime('%Y%m%d')
    temp_zip = './DK/data.zip'
    unzip_dir = f'./DK/Data-Epidemiologiske-Rapport-{today}/'
    url='https://covid19.ssi.dk/overvagningsdata/download-fil-med-overvaagningdata'
    if os.path.exists(unzip_dir):
        print(f"{unzip_dir} already exists. Update Skipped")
        return

    template = re.compile('data-epidemiologiske-rapport-[0-9]*-[a-z,0-9]*')

    SSI = requests.get(url)
    html_soup = BeautifulSoup(SSI.content, 'html.parser')
    LAST = [ a['href'] for n,a in enumerate(html_soup.find_all('a')) if template.findall(a['href']) ][0]

    with open(temp_zip,'wb') as fo:
        r = requests.get(LAST)
        fo.write(r.content)

    zdata = zipfile.ZipFile(temp_zip)
    os.mkdir(unzip_dir)
    zdata.extractall(path=unzip_dir)
    zdata.close()

if __name__ == "__main__":
    main()