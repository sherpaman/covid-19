import json
import os
import numpy as np
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go


def crop_and_mark(img_f, b, mark, font_file='/usr/share/fonts/truetype/msttcorefonts/arial.ttf'):
    i = Image.open(img_f).crop(box=b)
    draw = ImageDraw.Draw(i)
    font = ImageFont.truetype(font_file, 16)
    draw.text((300, 650), mark, (0, 0, 0), font=font)
    return i


def main():
    with open('./denmark-municipalities.geojson', 'r') as f:
        DK = json.load(f)

    for i in DK['features']:
        i['properties']['name'] = i['properties']['name'].split()[0]

    data_rate_ts = pd.read_json('./DK_ts.json')

    data_plot = data_rate_ts
    data_plot['rate_log10'] = data_plot.rolling_rate.apply(np.log10)
    dates = data_plot.date_sample.unique()

    fig = px.choropleth_mapbox(data_plot, geojson=DK, color='rate_log10',
                               locations="Navn", featureidkey="properties.name",
                               center={"lat": 56.15, "lon": 10.2107},
                               range_color=[-3, 1],
                               animation_frame='date_sample',
                               mapbox_style="carto-positron", zoom=6,
                               color_continuous_scale="reds",
                               width=800,
                               height=800)

    nfr = len(fig.frames)
    i_list = []

    if not os.path.exists('./tmp'):
        os.mkdir('./tmp')

    for n in range(nfr):
        file_name = f'./tmp/DK_{n:03d}.png'
        print(f'Generating Frame {n:03d}/{nfr:03d}')
        go.Figure(fig.frames[n].data, fig.layout).write_image(file_name)
        i_list.append(crop_and_mark(file_name, (0, 0, 800, 700), f"{dates[n]}"))

    img, *imgs = i_list
    img.save('DK_ts.gif', format='GIF', append_images=imgs,
             save_all=True, duration=240, loop=1)


if __name__ == "__main__":
    main()
