# COVID-19

Produce visual of COVID-19 evolution in Denmark

## MAP of Danish Municipalties Infection Rates

![Denmark Municipalities Covid-19 infection rates](./DK_ts.gif)

## IMPORTANT !
Maps are generated using plotly, which requires the manual installation of 
*Orca* an Electron app that generates images and reports of Plotly things like plotly.js graphs, dash apps, dashboards from the command line.

see https://github.com/plotly/orca for installation instructions.

### TESTED ON UBUNTU 20.04

- Download the standalone Orca binaries corresponding to
your operating system from the
[release](https://github.com/plotly/orca/releases) page.

- Make the orca AppImage executable.

```
$ chmod +x orca-X.Y.Z-x86_64.AppImage
```

- Create a symbolic link named `orca` somewhere on your `PATH` that points
to the AppImage.

```
$ ln -s /path/to/orca-X.Y.Z-x86_64.AppImage /somewhere/on/PATH/orca
```

- Open a new terminal and verify that the orca executable is available on your `PATH`.

```
$ which orca
/somewhere/on/PATH/orca

$ orca --help
Plotly's image-exporting utilities

  Usage: orca [--version] [--help] <command> [<args>]
  ...
```
